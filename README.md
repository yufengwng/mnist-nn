This is a very minimal and suboptimal neural network I wrote for a machine
learning intro class. It is meant for handwritten digit recognition and trained
with the MNIST dataset found on Yann LeCun's [website](http://yann.lecun.com/exdb/mnist/).

The model uses:

* one hidden layer
* Nguyen-Widrow weight initialization
* logistic sigmoid activation function
* Mean Squared Error cost function
* mini-batch stochastic backpropagation

To try it out, grab the dataset from the website and parse it into csv files
using the `preprocess.py` script. Then train the model:

```python
>>> import model
>>>
>>> train_set = model.load_dataset(TRAIN_DATA_CSV_PATH)
>>> nn = model.NeuralNetwork(784, 30, 10)
>>> nn.init_weights()
>>> nn.train(train_set, 50, 100)
```

And then we can evaluate performance:

```python
>>> labels, images = model.load_images(TEST_DATA_CSV_PATH)
>>> labels = model.vectorize_labels(labels)
>>> images = model.normalize_images(images)
>>> correct = nn.evaluate(images, labels)
>>> print("Accuracy: {}%".format(correct / len(images)))
```

