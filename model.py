#!/usr/bin/python3

import numpy as np
import pandas as pd
import time


def load_images(csv_path):
    """Returns the labels and image data read from the csv file."""
    dataset = pd.read_csv(csv_path)
    labels = np.array(dataset.iloc[:,0])
    images = np.array(dataset.iloc[:,1:])
    return labels, images


def load_dataset(csv_path):
    """Returns ndarray of read data from csv file."""
    dataset = pd.read_csv(csv_path)
    dataset = np.array(dataset)
    return dataset


def normalize_images(data):
    """Returns image data normalized between [0.0, 1.0], assuming pixels are in [0, 255]."""
    data = data.astype(np.float)
    data = np.multiply(data, 1.0 / 255.0)
    return data


def vectorize_labels(labels):
    """Returns an ndarray with only one element as 1, in the index of digit."""
    vectors = []
    for d in labels:
        vec = np.zeros(10)
        vec[d] = 1.0
        vectors.append(vec)
    return np.array(vectors)


def sigmoid(z):
    """Apply sigmoid activation function."""
    return 1.0 / (1.0 + np.exp(-z))


def sigmoid_prime(z):
    """Apply the derivative of the sigmoid function."""
    return sigmoid(z) * (1.0 - sigmoid(z))


class NeuralNetwork(object):

    def __init__(self, in_units, hidden_units, out_units):
        """Returns a new 3-layer neural network with the specified layer sizes."""

        # Hyper parameters
        self.input_size = in_units
        self.output_size = out_units
        self.hidden_size = hidden_units

        # Learning parameters
        self.rate = 3.0

        # Weight parameters, randomly initialized
        self.W1 = np.random.uniform(-0.5, 0.5, (self.input_size, self.hidden_size))
        self.W2 = np.random.uniform(-0.5, 0.5, (self.hidden_size, self.output_size))

    def configure(self, rate=None):
        """Change the learning parameters of the network."""
        self.rate = self.rate if rate is None else rate

    def init_weights(self):
        """Initialize weights using Nguyen-Widrow."""
        self.W1 = np.random.uniform(-0.5, 0.5, (self.input_size, self.hidden_size))
        self.W2 = np.random.uniform(-0.5, 0.5, (self.hidden_size, self.output_size))

        # Initialize the hidden layer weights
        beta = 0.7 * (self.hidden_size ** (1.0 / self.input_size))
        for n in range(self.hidden_size):
            norm_val = np.linalg.norm(self.W1[:,n])
            self.W1[:,n] = np.multiply(self.W1[:,n], beta / norm_val)

        # Initialize the output layer weights
        beta = 0.7 * (self.output_size ** (1.0 / self.hidden_size))
        for n in range(self.output_size):
            norm_val = np.linalg.norm(self.W2[:,n])
            self.W2[:,n] = np.multiply(self.W2[:,n], beta / norm_val)

    def forward(self, sample):
        """Forward propagation through the network.
        sample: ndarray of shape (n, input_size), where n is number of samples
        """
        self.Z2 = np.dot(sample, self.W1)
        self.A2 = sigmoid(self.Z2)
        self.Z3 = np.dot(self.A2, self.W2)
        self.y_hat = sigmoid(self.Z3)
        return self.y_hat

    def cost(self, estimate, target):
        """Sum Squared Error cost function.
        estimate: ndarray of shape (n, output_size), where n is number of samples
        target  : ndarray of shape (n, output_size)
        """
        return np.mean(np.mean((target - estimate) ** 2, axis=0))

    def cost_prime(self, sample, target, estimate):
        """Gradient descent derivative.
        sample  : ndarray of shape (n, input_size), where n is number of samples
        target  : ndarray of shape (n, output_size)
        estimate: ndarray of shape (n, output_size)
        """
        total = len(sample)

        delta3 = np.multiply(-(target - estimate), sigmoid_prime(self.Z3))
        dW2 = np.multiply(np.dot(self.A2.T, delta3), 2 / total)

        delta2 = np.dot(delta3, self.W2.T) * sigmoid_prime(self.Z2)
        dW1 = np.multiply(np.dot(sample.T, delta2), 2 / total)

        return dW1, dW2

    def evaluate(self, sample, target):
        """Evaluate network performace using given data."""
        results = self.forward(sample)
        pairs = [(np.argmax(x), np.argmax(y)) for x, y in zip(results, target)]
        correct = sum(int(x == y) for x, y in pairs)
        return correct

    def backprop(self, images, labels):
        """Update weights using batch backpropagation."""
        size = len(labels)
        dW1s = []
        dW2s = []

        for i in range(size):
            label = labels[i,:].reshape((1, self.output_size))
            image = images[i,:].reshape((1, self.input_size))

            estimate = self.forward(image)
            dW1, dW2 = self.cost_prime(image, label, estimate)

            dW1s.append(dW1)
            dW2s.append(dW2)

        self.W1 = self.W1 - (self.rate / size) * sum(dW1s)
        self.W2 = self.W2 - (self.rate / size) * sum(dW2s)

    def train(self, train_set, epochs, batch_size, test_set=None):
        """Train the neural network using given data and parameters."""
        if test_set is not None:
            size_test = len(test_set)
        size = len(train_set)
        print("num training data: {}".format(size))

        self.costs = []
        start = time.time()
        for r in range(epochs):
            np.random.shuffle(train_set)
            mini_batches = [train_set[i:i+batch_size] for i in range(0, size, batch_size)]

            for batch in mini_batches:
                labels = vectorize_labels(batch[:,0])
                images = normalize_images(batch[:,1:])
                self.backprop(images, labels)

            target = vectorize_labels(train_set[:,0])
            sample = normalize_images(train_set[:,1:])
            estimate = self.forward(sample)
            cost = self.cost(estimate, target)
            self.costs.append(cost)
            print("Epoch {} complete: cost {}".format(r, cost))

            if test_set is not None:
                target = vectorize_labels(test_set[:,0])
                sample = normalize_images(test_set[:,1:])
                correct = self.evaluate(sample, target)[0]
                print("  {} / {}".format(correct, size_test))
        stop = time.time()
        elapsed = stop - start
        print("Time elapsed: {} sec".format(elapsed))

