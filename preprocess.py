#!/usr/bin/python3
#
# This script preprocesses image dataset into csv format
# that is easily importable into python for use.
#
# See Yann LeCun's website for dataset:
# http://yann.lecun.com/exdb/mnist/

import gzip
import os.path
import pandas
import struct
import sys


def export(labels_path, images_path, output_file):
    """Read labels and data from given paths and dump csv to specified output file."""

    print("Reading in labels...")
    with gzip.open(labels_path, 'rb') as f:
        raw = f.read()
    magic, num_labels = struct.unpack('>ii', raw[0:8])
    fmt = 'B' * num_labels
    labels = struct.unpack(fmt, raw[8:])
    labels = list(labels)
    print("  {} labels".format(num_labels))

    print("Reading in images...")
    with gzip.open(images_path, 'rb') as f:
        raw = f.read()

    magic, num_imgs = struct.unpack('>ii', raw[0:8])
    num_rows, num_cols = struct.unpack('>ii', raw[8:16])
    num_pixels = num_rows * num_cols
    print("  {} images, {} pixels each".format(num_imgs, num_pixels))

    raw = raw[16:]
    fmt = 'B' * num_pixels

    images = []
    for i in range(num_imgs):
        img = struct.unpack(fmt, raw[0:num_pixels])
        record = list(img)
        record.insert(0, labels[i])
        images.append(record)
        raw = raw[num_pixels:]

    print("Dumping to {}...".format(output_file))
    col_names = ['label'] + ['px' + str(i) for i in range(num_pixels)]
    data = pandas.DataFrame(images, columns=col_names)
    data.to_csv(output_file, index=False)

    print("Done")


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: {} LABELS_FILE IMAGES_FILE OUTPUT_FILE".format(sys.argv[0]))
        sys.exit(1)

    labels = sys.argv[1]
    images = sys.argv[2]
    output = sys.argv[3]

    if not os.path.isfile(labels):
        print("{}: labels file does not exist".format(sys.argv[0]))
        sys.exit(1)
    elif not os.path.isfile(images):
        print("{}: images file does not exist".format(sys.argv[0]))
        sys.exit(1)

    if os.path.isfile(output):
        ans = input("Output file '{}' already exists, overwrite? [yN] ".format(output)).strip()
        if ans != 'y':
            sys.exit(0)

    export(labels, images, output)
